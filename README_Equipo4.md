# Proyecto Buildroot

Liga al archivo ova: https://drive.google.com/open?id=1CsUt4F9ZMTSdWmLdROgbtQF1EK7p_N5t

Alumnos:

Naomi Itzel Reyes Granados
Adriana Sánchez del Moral
José Manuel Martínez Sánchez

Configuracion de Buildroot

  - make list-defconfigs
  - "make pc_x86_64_bios_defconfig"
  - Configutacion en "make menuConfig"
  - Creacion de la imagen con "make"
  - Carga de imagen en VirtualBox
  - Carga de la aplicacion.

# MenuConfig

Como ya habiamos escogido una configuracion de list-defconfigs, la configuracion dentro de menuconfig fue unicamente para cargar paqueterias y bibliotecas.
En "Interpreter languages and scripting" solo se activo la opcion para PHP con FPM. En "databases" se activo la version para MySQL con Oracle, ademas se configuro la opcion FilesystemImages->exact size a 120 M porque con un tamaño mas chico hay varios paquetes que no se cargaron, como: "MySQL server"(de Oracle).

# Creacion de la imagen con "make"
Con el comando "make" podemos obtener la imagen, sin embargo esta no es posibe subir a la maquina virtual, por lo que con el comando "qemu-img convert output/images/disk.img -O vmdk output/images/disk.vmdk" podemos cambiarla a una imagen con extencion .vmdk.

# Carga de kernel en VirtualBox
Para la creacion de una maquina virtual con el kernel anteriormente creado, necesitamos poner (en nuestro caso particular) el tipo "Linux" con version "Other linux (64-bit) " con 1024M de espacio y seleccionamos nuetro archivo "disk.vdmk" como disco virtual de la maquina y empezamos la ejecucion.

# Carga de la aplicacion
Primero hacemos la conección remota de la maquina virtual a la nuestra, cambiamos configuracion de conección en virtual box(network) bridged Adapter y configuramos /etc/ssh/sshd.config; con esto nos conectamos desde host a target. Aqui pasamos los archivos de la aplicion y los guardamos en /var/www/, con el comando scp -r ../dummy_app_users root@192.168.169.110 /var/www/.
Luego modificamos /etc/nginx/nginx.config, para cambiarle la configuracion de red, primero descomentamos la funcion que recibe los php,luego modificamos para que buscara los archivos a ejecutar en /var/www/dummy_app_users/public, tanto php como el general. 
Y con esto se ouede ejecutar la apicacion con la url: 192.168.169.110/index.php
# Usuario y contraseña
- usuario: root
- contraseña: project2
